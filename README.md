# autonumber-for-vscode README
> 自动编号工具 使用方法 右键点击 AutoNumber菜单
* 只在 md 文件中有菜单
* 只格式化 `## Tag. 1: Title`
* 正则表达式为 `/(## [\s\S]*?\. )[\d\d].?(:)/`

## Step. 1: 这个插件的需求
> 写vitepress的markdown 发现 `## Step. 1: Title`这种形式挺好的
* 每个页面有很多后，上下调整顺序手改数字比较麻烦
* 所以写了这个插件

## Step. 2: 代码原理
> 用正则寻找 `##`开头 后面带 `. 数字:` 这个格式，然后替换数字

## 我的文档库
* https://pengchenggang.gitee.io/vuejsdev-com-gitee/
