// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode')

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate (context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "autonumber-for-vscode" is now active!')

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('autonumber-for-vscode.autoNumber', function () {
		// The code you place here will be executed every time your command is executed

		console.info('执行拉~~')
		// console.info('document', editor.document.getText())
		// console.info('document', editor.document)
		vscode.window.activeTextEditor.edit(editBuilder => {
			// 从开始到结束，全量替换
			const end = new vscode.Position(vscode.window.activeTextEditor.document.lineCount + 1, 0)
			const str = vscode.window.activeTextEditor.document.getText()
			const text = autoNumberFunc(str)
			editBuilder.replace(new vscode.Range(new vscode.Position(0, 0), end), text)
			// Display a message box to the user
			vscode.window.showInformationMessage('AutoNumber 替换二级编号已完成！')
		})
	})

	context.subscriptions.push(disposable)
}

// this method is called when your extension is deactivated
function deactivate () { }

function autoNumberFunc (data) {
	const arr = data.split('\r\n')
	let index = 1
	// 二级标题格式
	// ## Plug. 21: Path Intellisense
	const reg = /(## [\s\S]*?\. )[\d\d].?(:)/
	const retArr = arr.map(item => {
		if (reg.test(item)) {
			const c_item = item
			const str = item.replace(reg, "$1" + index + "$2")
			const c_str = str
			if (c_item !== c_str) {
				console.info('old: ', c_item)
				console.info('new: ', c_str)
				console.info('---')
			}
			index = index + 1
			return str
		} else {
			return item
		}
	})
	const outStr = retArr.join('\r\n')
	return outStr
}

module.exports = {
	activate,
	deactivate
}
